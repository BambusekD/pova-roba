#!/usr/bin/env python

import numpy as np

import readchar
import sys
import select
import termios
import tty
import math

from art_utils import ArtCalibrationHelper
from tf import TransformBroadcaster, transformations
import rospy
from geometry_msgs.msg import Transform
from art_calibration import ArtRobotCalibration, ArtCellCalibration
from std_msgs.msg import Bool
from art_msgs.srv import RecalibrateCell, RecalibrateCellRequest, RecalibrateCellResponse


class ArtCalibration(object):

    def __init__(self):

        self.cells = []

        self.cells.append(ArtRobotCalibration('pr2', '/pr2/ar_pose_marker',
                                              '/odom_combined', '/marker_detected'))

        for cell in rospy.get_param("cells", ["n1", "n2"]):
            self.cells.append(ArtCellCalibration(cell, '/art/' + cell + '/ar_pose_marker',
                                                 '/' + cell + '_link', '/marker_detected'))

        self.calibrated_pub = rospy.Publisher('/art/system/calibrated', Bool,
                                              queue_size=10, latch=True)
        self.calibrated = Bool()
        self.calibrated.data = False
        self.calibrated_sended = False
        self.calibrated_pub.publish(self.calibrated)
        self.recalibrate_cell_service = rospy.Service("/art/system/calibrate_cell", RecalibrateCell,
                                                      self.recalibrate_cell_cb)

        self.broadcaster = TransformBroadcaster()

    def recalibrate_cell_cb(self, req):
        resp = RecalibrateCellResponse()
        resp.success = False
        cell_name = req.cell_name
        for cell in self.cells:
            if cell.cell_id == cell_name:
                cell.reset_markers_searching()
                resp.success = True
                break
        else:
            resp.error = "Unknown cell"
        return resp

    def publish_calibration(self, manualTr, manualRot):
        calibrated = True

        time = rospy.Time.now() + rospy.Duration(1.0 / 30)

        for cell in self.cells:
            if cell.calibrated:
                tr = cell.get_transform()
                if cell.cell_id=='n1':
                    # add manually obtained values to translation matrix
                    tr.translation[0] += manualTr.x
                    tr.translation[1] += manualTr.y
                    tr.translation[2] += manualTr.z
                    tr.rotation = (quaternion_multiply(tr.rotation, manualRot.getQ()))

                self.broadcaster.sendTransform(tr.translation, tr.rotation,
                                               time, cell.world_frame,
                                               cell.cell_frame)

            else:
                calibrated = False

        if calibrated and not self.calibrated_sended:
            self.calibrated_sended = True
            self.calibrated.data = True
            self.calibrated_pub.publish(self.calibrated)

class ManualQuaternion():
    def __init__(self):
        self.x = 0
        self.y = 0
        self.z = 0
        self.w = 1
        # rotation step
        self.rot_step = 0.01

    def normalizeQ(self):
        xSq = self.x*self.x
        ySq = self.y*self.y
        zSq = self.z*self.z
        wSq = self.w*self.w
        acc = xSq + ySq + zSq + wSq
        sqr = math.sqrt(acc)
        self.x /= sqr
        self.y /= sqr
        self.z /= sqr
        self.w /= sqr

    def rotateQ_add_x(self):
        self.x += self.rot_step
        self.normalizeQ()
    def rotateQ_minus_x(self):
        self.x -= self.rot_step
        self.normalizeQ()

    def rotateQ_add_y(self):
        self.y += self.rot_step
        self.normalizeQ()
    def rotateQ_minus_y(self):
        self.y -= self.rot_step
        self.normalizeQ()

    def rotateQ_add_z(self):
        self.z += self.rot_step
        self.normalizeQ()
    def rotateQ_minus_z(self):
        self.z -= self.rot_step
        self.normalizeQ()
    def getQ(self):
        return[self.x,self.y,self.z,self.w]

def normalizeQ(inputQ):
    xSq = inputQ[0]*inputQ[0]
    ySq = inputQ[1]*inputQ[1]
    zSq = inputQ[2]*inputQ[2]
    wSq = inputQ[3]*inputQ[3]
    acc = xSq + ySq + zSq + wSq
    sqr = math.sqrt(acc)
    inputQ[0] /= sqr
    inputQ[1] /= sqr
    inputQ[2] /= sqr
    inputQ[3] /= sqr
    return inputQ

def quaternion_multiply(quaternion1, quaternion0):
    b1, c1, d1, a1 = quaternion0
    b2, c2, d2, a2 = quaternion1
    return np.array([a1*b2 + b1*a2 + c1*d2 - d1*c2,
                     a1*c2 - b1*d2 + c1*a2 + d1*b2,
                     a1*d2 + b1*c2 - c1*b2 + d1*a2,
                     a1*a2 - b1*b2 - c1*c2 - d1*d2], dtype=np.float64)

class ManualTranslation():
    def __init__(self):
        self.x = 0.0
        self.y = 0.0
        self.z = 0.0
        # translation step
        self.step = 0.005

    def add_x(self):
        self.x += self.step
    def minus_x(self):
        self.x -= self.step
    def add_y(self):
        self.y += self.step
    def minus_y(self):
        self.y -= self.step
    def add_z(self):
        self.z += self.step
    def minus_z(self):
        self.z -= self.step

def isData():
    return select.select([sys.stdin], [], [], 0) == ([sys.stdin], [], [])


if __name__ == '__main__':
    rospy.init_node('art_calibration', log_level=rospy.INFO)

    # manually edit calibration
    manualTr = ManualTranslation()
    manualRot = ManualQuaternion()

    try:
        node = ArtCalibration()

        rate = rospy.Rate(30)

        old_settings = termios.tcgetattr(sys.stdin)
        try:
            tty.setcbreak(sys.stdin.fileno())

            while not rospy.is_shutdown():
                node.publish_calibration(manualTr, manualRot)

                #reset manual edit
                manualRot = ManualQuaternion()
                manualTr = ManualTranslation()

                if isData():
                    c = sys.stdin.read(1)
                    if c == '\x1b':         # x1b is ESC
                        break
                    # translation
                    elif c == 'a':
                        manualTr.minus_x()
                    elif c == 'd':
                        manualTr.add_x()
                    elif c == 's':
                        manualTr.minus_y()
                    elif c == 'w':
                        manualTr.add_y()
                    elif c == 'q':
                        manualTr.minus_z()
                    elif c == 'e':
                        manualTr.add_z()
                    # rotation
                    elif c == 'j':
                        manualRot.rotateQ_minus_x()
                    elif c == 'l':
                        manualRot.rotateQ_add_x()
                    elif c == 'k':
                        manualRot.rotateQ_minus_y()
                    elif c == 'i':
                        manualRot.rotateQ_add_y()
                    elif c == 'u':
                        manualRot.rotateQ_minus_z()
                    elif c == 'o':
                        manualRot.rotateQ_add_z()

                    print('currentManualTranslation:\n')
                    print('['+ str(manualTr.x) + ', ' + str(manualTr.y) + ', ' + str(manualTr.z) + ']')
                    print('currentManualRotation:\n')
                    print('[' + str(manualRot.x) + ', ' + str(manualRot.y) + ', ' + str(manualRot.z) + ', ' + str(manualRot.w) + ']')

                rate.sleep()
        finally:
            termios.tcsetattr(sys.stdin, termios.TCSADRAIN, old_settings)

    except rospy.ROSInterruptException:
        pass
