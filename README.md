### What is this repository for? ###

* This is a project to Robotics containing the set up and detection od objects from two Kinects version 1.

### How do I get set up? ###

## Set up of environment: ##
1. Prepare two kinects and set up them to have 90�-160� angle between them and they point to ground.
2. Place markers number 10, 11, 12 and 13 on the ground as on image below.  
	![markers](/images/markers.jpg)

## Calibration ##

1. Run roscore.
2. Run kinects by command:  
    _roslaunch twokinectcalibration detectObjects\_runkinects.launch_
3. The calibration can be done with rgb or ir cameras of kinects (using ir cameras is highly recomended).  
    i. To run calibration using ir cameras:  
	    _roslaunch ir_image_preprocess ir_image_preprocess.launch_  
	    _roslaunch twokinectcalibration art_calibration_ir.launch_  
    ii. To run calibration using rgb cameras:  
	    _roslauch twokinectcalibration art_calibration.launch_  
4. If the result of calibration isn't sufficient, there is option to edit it manually. While the command line with calibration is active, the calibration can be edited usign keyboard as described below.  
    a,d - translation by x axis  
	s,w - translation by y axis  
	q,e - translation by z axis  
    j,l - rotation by x axis  
	k,i - rotation by y axis  
	u,o - rotation by z axis  

## Object Detection ##

1. Combine point clouds from kinects:  
    _rosrun get_pointclouds_delay merge_clouds_  
2. To detect objects in scene run objects detector node:    
    _rosrun objects_detector objects_detector  
3. The positions of objects are broadcast usign tf library.


### Who do I talk to? ###

For more info email:

* xkruta03 (at) vutbr.cz
* xricht19 (at) vutbr.cz
* xbambu04 (at) vutbr.cz