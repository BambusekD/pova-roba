#include <ros/ros.h>
#include <ros/package.h>
#include <sensor_msgs/PointCloud2.h>
#include <pcl_conversions/pcl_conversions.h>
#include <sstream>
#include <iostream>
#include <boost/filesystem/operations.hpp>
#include <boost/filesystem/path.hpp>
#include <tf/transform_listener.h>
#include <tf/transform_broadcaster.h>
#include <pcl_ros/transforms.h>

#include <pcl/ModelCoefficients.h>
#include <pcl/point_types.h>
#include <pcl/io/pcd_io.h>
#include <pcl/io/ply_io.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/features/normal_3d.h>
#include <pcl/kdtree/kdtree.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/segmentation/extract_clusters.h>
#include <pcl/registration/icp.h>
#include <pcl/filters/statistical_outlier_removal.h>
#include <pcl/common/common.h>
#include <pcl/common/distances.h>
#include <pcl/filters/passthrough.h>
#include <pcl/segmentation/region_growing_rgb.h>

#include <objects_detector/model.h>

#include <objects_detector/tfTransform.h>
#include <objects_detector/ObjTfsMsg.h>

#define DIMENSION_TOLERANCE 0.04

static ros::Publisher pub_result;
static ros::Publisher pub_plane;
static ros::Publisher pub_objTfs;

using namespace std;
namespace fs = boost::filesystem;


void filterCloud(pcl::PointCloud<pcl::PointXYZ>::Ptr &cloud_in, pcl::PointCloud<pcl::PointXYZ>::Ptr &cloud_filtered) {
    //std::cout << "PointCloud before filtering has: " << cloud_in->points.size() << " data points." << std::endl;
    // Create the filtering object: downsample the dataset using a leaf size of 1cm
    pcl::VoxelGrid<pcl::PointXYZ> vg;
    vg.setInputCloud(cloud_in);
    vg.setLeafSize(0.01f, 0.01f, 0.01f);
    vg.filter(*cloud_filtered);
    //std::cout << "PointCloud after filtering has: " << cloud_filtered->points.size() << " data points." << std::endl;
}

void segmentPlanes(pcl::PointCloud<pcl::PointXYZ>::Ptr &cloud_in, pcl::PointCloud<pcl::PointXYZ>::Ptr &cloud_out) {

    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_plane(new pcl::PointCloud<pcl::PointXYZ>);
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_copy(new pcl::PointCloud<pcl::PointXYZ>);
    *cloud_copy = *cloud_in;
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_f(new pcl::PointCloud<pcl::PointXYZ>);

    pcl::ModelCoefficients::Ptr coefficients(new pcl::ModelCoefficients);
    pcl::PointIndices::Ptr inliers(new pcl::PointIndices);
    // Create the segmentation object
    pcl::SACSegmentation<pcl::PointXYZ> seg;
    seg.setOptimizeCoefficients(true);
    seg.setModelType(pcl::SACMODEL_PLANE);
    seg.setMethodType(pcl::SAC_RANSAC);
    seg.setDistanceThreshold(0.01);

//    pcl::PCDWriter writer;

    int nr_points = (int) cloud_copy->points.size();
    while (cloud_copy->points.size() > 0.3 * nr_points) {
        // Segment the largest planar component from the remaining cloud
        seg.setInputCloud(cloud_copy);
        seg.segment(*inliers, *coefficients);
        if (inliers->indices.size() == 0) {
            std::cout << "Could not estimate a planar model for the given dataset." << std::endl;
            break;
        }

        // Extract the planar inliers from the input cloud
        pcl::ExtractIndices<pcl::PointXYZ> extract;
        extract.setInputCloud(cloud_copy);
        extract.setIndices(inliers);
        extract.setNegative(false);

        // Get the points associated with the planar surface
        extract.filter(*cloud_plane);
        //std::cout << "PointCloud representing the planar component: " << cloud_plane->points.size() << " data points."
         //         << std::endl;

//        std::stringstream ss;
//        ss << "plane" << cloud_plane->points.size() << ".pcd";
//        writer.write<pcl::PointXYZ>(ss.str(), *cloud_plane, false);

        // Remove the planar inliers, extract the rest
        extract.setNegative(true);
        extract.filter(*cloud_f);
        *cloud_copy = *cloud_f;
    }

    *cloud_out = *cloud_copy;


    sensor_msgs::PointCloud2::Ptr detected_plane(new sensor_msgs::PointCloud2);
    pcl::toROSMsg(*cloud_plane, *detected_plane);
    //detected_plane->header.frame_id = "marker";
    // Publish the data
    pub_plane.publish(detected_plane);
}

void segmentPlane(pcl::PointCloud<pcl::PointXYZ>::Ptr &cloud_in, pcl::PointCloud<pcl::PointXYZ>::Ptr &cloud_out) {

    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_plane(new pcl::PointCloud<pcl::PointXYZ>);

    pcl::ModelCoefficients::Ptr coefficients(new pcl::ModelCoefficients);
    pcl::PointIndices::Ptr inliers(new pcl::PointIndices);
    // Create the segmentation object
    pcl::SACSegmentation<pcl::PointXYZ> seg;
    seg.setOptimizeCoefficients(true);
    seg.setModelType(pcl::SACMODEL_PLANE);
    seg.setMethodType(pcl::SAC_RANSAC);
    seg.setDistanceThreshold(0.02);
    seg.setInputCloud(cloud_in);
    seg.segment(*inliers, *coefficients);


    // Extract the planar inliers from the input cloud
    pcl::ExtractIndices<pcl::PointXYZ> extract;
    extract.setInputCloud(cloud_in);
    extract.setIndices(inliers);
    extract.setNegative(false);

    // Get the points associated with the planar surface
    extract.filter(*cloud_plane);
    //std::cout << "PointCloud representing the planar component: " << cloud_plane->points.size() << " data points."
    //          << std::endl;

    // Remove the planar inliers, extract the rest
    extract.setNegative(true);
    extract.filter(*cloud_out);
}


std::vector<pcl::PointCloud<pcl::PointXYZ> > clusterExtraction(pcl::PointCloud<pcl::PointXYZ>::Ptr &cloud_in) {
    //pcl::PCDWriter writer;

    std::vector<int> indices;
    pcl::removeNaNFromPointCloud(*cloud_in, *cloud_in, indices);

    //writer.write<pcl::PointXYZ>("input_cloud.pcd", *cloud_in, false);

    // Creating the KdTree object for the search method of the extraction
    pcl::search::KdTree<pcl::PointXYZ>::Ptr tree(new pcl::search::KdTree<pcl::PointXYZ>);
    tree->setInputCloud(cloud_in);

    std::vector<pcl::PointIndices> cluster_indices;
    pcl::EuclideanClusterExtraction<pcl::PointXYZ> ec;
    ec.setClusterTolerance(0.02); // 2cm
    ec.setMinClusterSize(100);
    ec.setMaxClusterSize(25000);
    ec.setSearchMethod(tree);
    ec.setInputCloud(cloud_in);
    ec.extract(cluster_indices);

    std::vector<pcl::PointCloud<pcl::PointXYZ> > clusters;

    int j = 0;
    for (std::vector<pcl::PointIndices>::const_iterator it = cluster_indices.begin();
         it != cluster_indices.end(); ++it) {
        pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_cluster(new pcl::PointCloud<pcl::PointXYZ>);
        pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_segmented(new pcl::PointCloud<pcl::PointXYZ>);
        for (std::vector<int>::const_iterator pit = it->indices.begin(); pit != it->indices.end(); ++pit)
            cloud_cluster->points.push_back(cloud_in->points[*pit]);
        cloud_cluster->width = (uint32_t) cloud_cluster->points.size();
        cloud_cluster->height = 1;
        cloud_cluster->is_dense = true;

//        //FILTERING OUTLIERS
//        // Create the filtering object
//        pcl::StatisticalOutlierRemoval<pcl::PointXYZ> sor;
//        sor.setInputCloud(cloud_cluster);
//        sor.setMeanK(50);
//        sor.setStddevMulThresh(1.0);
//        sor.filter(*cloud_cluster);

        //if(cloud_segmented->points.size() < cloud_cluster->points.size())



//        TODO COLOR BASED SEGMENTATION - probably wont be using this
//        pcl::PointCloud <pcl::PointXYZRGB>::Ptr cloud (new pcl::PointCloud <pcl::PointXYZRGB>);
//        pcl::IndicesPtr indices (new std::vector <int>);
//        pcl::PassThrough<pcl::PointXYZRGB> pass;
//        pass.setInputCloud (cloud);
//        pass.setFilterFieldName ("z");
//        pass.setFilterLimits (0.0, 1.0);
//        pass.filter (*indices);
//
//        pcl::RegionGrowingRGB<pcl::PointXYZRGB> reg;
//        reg.setInputCloud (cloud);
//        reg.setIndices (indices);
//        reg.setSearchMethod (tree);
//        reg.setDistanceThreshold (10);
//        reg.setPointColorThreshold (6);
//        reg.setRegionColorThreshold (5);
//        reg.setMinClusterSize (600);
//
//        std::vector <pcl::PointIndices> clusters;
//        reg.extract (clusters);
//
//        pcl::PointCloud <pcl::PointXYZRGB>::Ptr colored_cloud = reg.getColoredCloud ();



//        std::cout << "PointCloud representing the Cluster: " << cloud_cluster->points.size() << " data points."
//                  << std::endl;

        //segment plane from cluster
        segmentPlane(cloud_cluster, cloud_segmented);
        //if there left any points, then pass cluster for further processing
        if(!cloud_segmented->points.empty()) {
//            std::cout << "PointCloud before segmentation: " << cloud_cluster->points.size() << " data points."
//                      << std::endl;
//            std::cout << "PointCloud after segmentation: " << cloud_segmented->points.size() << " data points."
//                      << std::endl;
            //std::stringstream ss;
            //ss << "cloud_cluster_" << j << ".pcd";
            //writer.write<pcl::PointXYZ>(ss.str(), *cloud_cluster, false);
            j++;

            clusters.push_back(*cloud_cluster);
        }
    }
    //exit(EXIT_SUCCESS);
    return clusters;
}

bool checkDimensions(pcl::PointCloud<pcl::PointXYZ>::Ptr &cloud_cluster, pcl::PointCloud<pcl::PointXYZ>::Ptr &cloud_model) {
    pcl::PointXYZ *cluster_minPT(new pcl::PointXYZ);
    pcl::PointXYZ *cluster_maxPT(new pcl::PointXYZ);

    pcl::PointXYZ *model_minPT(new pcl::PointXYZ);
    pcl::PointXYZ *model_maxPT(new pcl::PointXYZ);

    //get minimum and maximum dimension (x,y,z) of pointcloud
    pcl::getMinMax3D(*cloud_cluster, *cluster_minPT, *cluster_maxPT);

    pcl::getMinMax3D(*cloud_model, *model_minPT, *model_maxPT);

    float cluster_x = cluster_maxPT->data[0] - cluster_minPT->data[0];
    float cluster_y = cluster_maxPT->data[1] - cluster_minPT->data[1];
    float cluster_z = cluster_maxPT->data[2] - cluster_minPT->data[2];

    float model_x = model_maxPT->data[0] - model_minPT->data[0];
    float model_y = model_maxPT->data[1] - model_minPT->data[1];
    float model_z = model_maxPT->data[2] - model_minPT->data[2];

//    float cluster_x = pcl::euclideanDistance(new pcl::PointXYZ(cluster_maxPT->data[0], 0, 0), new pcl::PointXYZ(cluster_minPT->data[0], 0, 0));
//    float cluster_y = pcl::euclideanDistance(new pcl::PointXYZ(0, cluster_maxPT->data[1], 0), new pcl::PointXYZ(0, cluster_minPT->data[1], 0));
//    float cluster_z = pcl::euclideanDistance(new pcl::PointXYZ(0, 0, cluster_maxPT->data[2]), new pcl::PointXYZ(0, 0, cluster_minPT->data[2]));
//
//    float model_x = pcl::euclideanDistance(new pcl::PointXYZ(model_maxPT->data[0], 0, 0), new pcl::PointXYZ(model_minPT->data[0], 0, 0));
//    float model_y = pcl::euclideanDistance(new pcl::PointXYZ(0, model_maxPT->data[1], 0), new pcl::PointXYZ(0, model_minPT->data[1], 0));
//    float model_z = pcl::euclideanDistance(new pcl::PointXYZ(0, 0, model_maxPT->data[2]), new pcl::PointXYZ(0, 0, model_minPT->data[2]));


    //cout << "Cluster x: " << cluster_x << ", y: " << cluster_y << ", z: " << cluster_z << endl;
    //cout << "Model x: " << model_x << ", y: " << model_y << ", z: " << model_z << endl;

    //cout << "Diff abs x: " << abs(cluster_x - model_x) << ", y: " << abs(cluster_y - model_y) << ", z: " << abs(cluster_z - model_z) << endl;

    if(abs(cluster_x - model_x) < DIMENSION_TOLERANCE && abs(cluster_y - model_y) < DIMENSION_TOLERANCE && abs(cluster_z - model_z) < DIMENSION_TOLERANCE) {
        return true;
    }
    //TODO chance that it's column of cubes
//    if(abs(cluster_x - model_x) < 0.01 && abs(cluster_y - model_y) < 0.01) {
//        return true;
//    }



    return false;
}

bool detectObjectFromCluster(pcl::PointCloud<pcl::PointXYZ>::Ptr &cluster,
                             std::vector<Model> &models, tf::StampedTransform & stfTf) {
    //pcl::PCDWriter writer;

    int iterations = 10;
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_icp(new pcl::PointCloud<pcl::PointXYZ>);

    //initial fitness score
    double fitness_score = 1000;
    // Defining a rotation matrix and translation vector
    Eigen::Matrix4f icp_transformation_matrix = Eigen::Matrix4f::Identity();
    //for best match
    //pcl::PointCloud<pcl::PointXYZ>::Ptr best_match(new pcl::PointCloud<pcl::PointXYZ>);
    Model bestModel;
    //traverse through all models and try to find best match for current cluster
    for (std::vector<Model>::const_iterator it = models.begin(); it != models.end(); ++it) {

        pcl::copyPointCloud(*it->getCloud(), *cloud_icp);

        // The Iterative Closest Point algorithm
        pcl::IterativeClosestPoint<pcl::PointXYZ, pcl::PointXYZ> icp;
        icp.setMaximumIterations(iterations);
        icp.setInputSource(cloud_icp);
        icp.setInputTarget(cluster);
        icp.align(*cloud_icp);

        //*cloud_icp += *cluster;

        if (icp.hasConverged()) {
            if(fitness_score > icp.getFitnessScore()) {
                //if dimensions approximately match
                if(checkDimensions(cluster, cloud_icp)) {
                    fitness_score = icp.getFitnessScore();
                    icp_transformation_matrix = icp.getFinalTransformation().cast<float>();
                    //*best_match = *cloud_icp;
                    bestModel.setName(it->getName());
                    bestModel.setCloud(cloud_icp);
                }
            }
        } else {
            PCL_ERROR ("\nICP has not converged.\n");
        }
    }

    //higher fitness score means it detected bullshit
    if(fitness_score < 0.3) {
        //save best match
        cout << "OBJECT FOUND! - " << bestModel.getName() << endl;
        cout << "ICP score is " << fitness_score << endl;
        //get fitness score to string (float -> string) for filename
        std::ostringstream strs;
        strs << fitness_score;
        std::string fitness_score_str = strs.str();

        pcl::PointCloud<pcl::PointXYZRGB> cloud;
        pcl::copyPointCloud(*bestModel.getCloud(), cloud);
        for(size_t i = 0; i < cloud.points.size(); i++)
        {
            cloud.points[i].r = 0;
            cloud.points[i].g = 255;
            cloud.points[i].b = 0;
        }

        //writer.write<pcl::PointXYZ>("object_" + bestModel.getName() + fitness_score_str + ".pcd", *bestModel.getCloud() + *cluster, false);
        //writer.write<pcl::PointXYZ>("object_" + bestModel.getName() + fitness_score_str + ".pcd", *cluster, false);
        //writer.write<pcl::PointXYZRGB>("model_" + bestModel.getName() + fitness_score_str + ".pcd", cloud, false);


        //PUBLISH TF
        //compute center of cloud
        Eigen::Vector4f centroid;
        pcl::compute3DCentroid(*cloud_icp, centroid);
        //get its rotation and translation
        Eigen::Matrix3f rotation = icp_transformation_matrix.block<3,3>(0, 0);
        Eigen::Vector3f translation = icp_transformation_matrix.block<3,1>(0, 3);
        //get angles around axis (x,y,z)
        Eigen::Vector3f angles = rotation.eulerAngles(0,1,2);

        //set result rotation for broadcasting tf frames
        tf::Quaternion q;
        q.setRPY(angles(0), angles(1), angles(2));

        tf::Transform object_tf;
        object_tf.setOrigin(tf::Vector3(centroid[0], centroid[1], centroid[2]));
        object_tf.setRotation(q);

        static tf::TransformBroadcaster br;
        stfTf =  tf::StampedTransform(object_tf, ros::Time::now(), "marker", bestModel.getName() + fitness_score_str);
        br.sendTransform(stfTf);
        return true;
    }
    return false;
}

void fillMsg(tf::Transform * transformation, objects_detector::tfTransform & msg)
{
    tf::Quaternion q = transformation->getRotation();
    tf::Vector3 origin = transformation->getOrigin();
    msg.originX = origin[0];
    msg.originY = origin[1];
    msg.originZ = origin[2];

    msg.quatX = q[0];
    msg.quatY = q[1];
    msg.quatZ = q[2];
    msg.quatW = q[3];
}

void callback_func(const sensor_msgs::PointCloud2ConstPtr &input, std::vector<Model> &models) {

    cout << "New pointcloud data received! .. trying to find objects.." << endl;

    // Read in the cloud data
    pcl::PointCloud<pcl::PointXYZ>::Ptr inputCloud(new pcl::PointCloud<pcl::PointXYZ>);
    pcl::fromROSMsg(*input, *inputCloud);
    pcl::PointCloud<pcl::PointXYZ>::Ptr segmentedCloud(new pcl::PointCloud<pcl::PointXYZ>);
    pcl::PointCloud<pcl::PointXYZ>::Ptr filteredCloud(new pcl::PointCloud<pcl::PointXYZ>);

    //downsample input cloud for faster processing
    filterCloud(inputCloud, filteredCloud);
    //segment all planes from cloud
    segmentPlanes(filteredCloud, segmentedCloud);
    std::vector<pcl::PointCloud<pcl::PointXYZ> > clusters;

    //get clusters of remaining cloud
    clusters = clusterExtraction(segmentedCloud);

    // create vector of scene objects tranformations
    std::vector<tf::StampedTransform> sceneObjsTfs;
    sceneObjsTfs.reserve(clusters.size());
    tf::StampedTransform transform;
    // create message pbject
    objects_detector::ObjTfsMsg detObjTfs;

    int objCnt = 1;
    //traverse through clusters and try to match best candidate for them from model database
    for (std::vector<pcl::PointCloud<pcl::PointXYZ> >::const_iterator it = clusters.begin();
         it != clusters.end(); ++it) 
    {
        pcl::PointCloud<pcl::PointXYZ>::Ptr cluster(new pcl::PointCloud<pcl::PointXYZ>);
        *cluster = *it;

        if(detectObjectFromCluster(cluster, models, transform)) {

            tf::Transform *transformation = &transform; // upcast to parent
            objects_detector::tfTransform tfTransformInfo;
            fillMsg(transformation, tfTransformInfo);
            tfTransformInfo.objID = "detObj_" + boost::to_string(objCnt); // add name to the object
            objCnt++;

            detObjTfs.tfObjects.push_back(tfTransformInfo);
            sceneObjsTfs.push_back(transform);
        }
    }
    detObjTfs.header.stamp = ros::Time::now();
    detObjTfs.header.frame_id = "/marker";

    static tf::TransformBroadcaster br;
    // send all tranforms at once not one by one
    br.sendTransform(sceneObjsTfs);
    // publish the vector of transforms
    pub_objTfs.publish(detObjTfs);

    sensor_msgs::PointCloud2::Ptr result(new sensor_msgs::PointCloud2);
    pcl::toROSMsg(*segmentedCloud, *result);
    //detected_plane->header.frame_id = "marker";
    // Publish the data
    pub_result.publish(result);
}


int main(int argc, char **argv) {
    /**
     * The ros::init() function needs to see argc and argv so that it can perform
     * any ROS arguments and name remapping that were provided at the command line.
     * For programmatic remappings you can use a different version of init() which takes
     * remappings directly, but for most command-line programs, passing argc and argv is
     * the easiest way to do it.  The third argument to init() is the name of the node.
     *
     * You must call one of the versions of ros::init() before using any other
     * part of the ROS system.
     */
    ros::init(argc, argv, "objects_detector");


    ros::NodeHandle n;

    //LOAD MODELS FROM {catkin_ws/src}/objects_detector/models/
    string path = ros::package::getPath("objects_detector").append("/models/");

    //vector of all models
    std::vector<Model> models;
    pcl::PointCloud<pcl::PointXYZ>::Ptr model_cloud(new pcl::PointCloud<pcl::PointXYZ>);
    //iterate through directory of models and append them to vector
    fs::directory_iterator end_iter;
    for (fs::directory_iterator dir_itr(path); dir_itr != end_iter; ++dir_itr) {
        cout << "Loading model " << dir_itr->path() << endl;
        pcl::io::loadPCDFile(dir_itr->path().c_str(), *model_cloud);
        //cout << replace(dir_itr->path().filename().begin(), dir_itr->path().filename().end(), ".pcd" )  << endl;
        //cout << dir_itr->path().stem().string() << endl;
        Model model;
        model.setName(dir_itr->path().stem().string());
        model.setCloud(model_cloud);
        models.push_back(model);
    }
    ros::Subscriber sub = n.subscribe<sensor_msgs::PointCloud2>("/kinect_merged/depth_registered/points", 1, boost::bind(callback_func, _1, models));
    //ros::Subscriber sub = n.subscribe<sensor_msgs::PointCloud2>("/kinect/points", 1, boost::bind(callback_func, _1, &listener));

    // publish the object transformation
    pub_objTfs = n.advertise<objects_detector::ObjTfsMsg>("objTransformations", 1);
    // Create a ROS publisher for the output point cloud
    //pub_original = n.advertise<pcl::PCLPointCloud2> ("original_cloud", 1);
    pub_plane = n.advertise<pcl::PCLPointCloud2>("plane_cloud", 1);

    pub_result = n.advertise<pcl::PCLPointCloud2>("detected_object_cloud", 1);

    // Spin
    ros::spin();

}
