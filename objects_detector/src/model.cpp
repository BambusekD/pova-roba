//
// Created by daniel on 6.12.17.
//

#include <objects_detector/model.h>


using namespace std;

Model::Model() {
    cloud = pcl::PointCloud<pcl::PointXYZ>::Ptr(new pcl::PointCloud<pcl::PointXYZ>);
};

void Model::setName(string _name) {
    name = _name;
}

void Model::setCloud(pcl::PointCloud<pcl::PointXYZ>::Ptr _cloud) {
    pcl::copyPointCloud(*_cloud, *cloud);
}

string Model::getName() const {
    return name;
}

pcl::PointCloud<pcl::PointXYZ>::Ptr Model::getCloud() const {
    return cloud;
}