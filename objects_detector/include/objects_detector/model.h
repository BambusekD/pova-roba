//
// Created by daniel on 11.12.17.
//

#ifndef PROJECT_MODEL_H
#define PROJECT_MODEL_H

#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
#include <pcl/common/io.h>
#include <string>

using namespace std;

class Model {
private:
    string name;
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud;
public:
    Model();
    void setName(string _name);
    void setCloud(pcl::PointCloud<pcl::PointXYZ>::Ptr _cloud);
    string getName() const;
    pcl::PointCloud<pcl::PointXYZ>::Ptr getCloud() const;
};


#endif //PROJECT_MODEL_H
