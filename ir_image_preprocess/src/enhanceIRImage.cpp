//
// Created by jirka on 6.12.17.
//

#include <iostream>
#include <ros/ros.h>
#include <ros/console.h>
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

static const std::string OPENCV_WINDOW = "Image window";

class IRImagePreprocess
{
    ros::NodeHandle nh_;
    image_transport::ImageTransport it_;
    image_transport::Subscriber image_sub_;
    image_transport::Publisher image_pub_;

public:
    IRImagePreprocess(char* inputTopic, char* outputTopic) : it_(nh_)
    {
        image_sub_ = it_.subscribe(inputTopic, 1, &IRImagePreprocess::imageProcess, this);
        image_pub_ = it_.advertise(outputTopic, 1);

        //cv::namedWindow(OPENCV_WINDOW);
    }

    ~IRImagePreprocess()
    {
         //cv::destroyWindow(OPENCV_WINDOW);
    }

    void imageProcess(const sensor_msgs::ImageConstPtr& msg)
    {
        cv_bridge::CvImagePtr cv_ptr;
        try {
            cv_ptr = cv_bridge::toCvCopy(msg,sensor_msgs::image_encodings::BGR8);
        }
        catch (cv_bridge::Exception& e){
            ROS_ERROR("cv_bridge exception: %s", e.what());
            return;
        }

        //cv::Mat thresholded;
        threshold(cv_ptr->image, cv_ptr->image, 0, 255, cv::THRESH_BINARY);
        //Mat element = getStructuringElement(MORPH_CROSS, Size(3,3));
        //morphologyEx(thresholded, thresholded, MORPH_OPEN, element);
        //morphologyEx(thresholded, thresholded, MORPH_CLOSE, element);

        // Update GUI Window
        //cv::imshow(OPENCV_WINDOW, cv_ptr->image);
        //cv::waitKey(3);

        // Output modified video stream
        image_pub_.publish(cv_ptr->toImageMsg());
    }
};


int main(int argc, char** argv)
{
    ros::init(argc, argv, "ir_image_preprocess_node");

    if (argc != 3){
        ROS_ERROR_STREAM("Not enough parameters for node!");
        return 1;
    }
    IRImagePreprocess irImPre(argv[1], argv[2]);
    ros::spin();
    return 0;
}