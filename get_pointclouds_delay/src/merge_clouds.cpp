#include <pcl/point_cloud.h>
#include <pcl_ros/transforms.h>
#include <pcl_conversions/pcl_conversions.h>
#include <ros/ros.h>
#include <tf/tf.h>
#include <tf/transform_listener.h>
#include <sensor_msgs/PointCloud2.h>
#include <ros/publisher.h>
#include <pcl/registration/icp.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/io/pcd_io.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/segmentation/sac_segmentation.h>


ros::Publisher pub;
ros::Subscriber sub1, sub2;
tf::TransformListener *listener;
//sensor_msgs::PointCloud2 output, output1, output2;
pcl::PointCloud<pcl::PointXYZRGB> output1_pcl, output2_pcl;

void cloud_cb1(const sensor_msgs::PointCloud2ConstPtr& input1);
void cloud_cb2(const sensor_msgs::PointCloud2ConstPtr& input2);


void filterCloud(pcl::PointCloud<pcl::PointXYZRGB>::Ptr &cloud_in, pcl::PointCloud<pcl::PointXYZRGB>::Ptr &cloud_filtered) {
    std::cout << "PointCloud before filtering has: " << cloud_in->points.size() << " data points." << std::endl;
    // Create the filtering object: downsample the dataset using a leaf size of 1cm
    pcl::VoxelGrid<pcl::PointXYZRGB> vg;
    vg.setInputCloud(cloud_in);
    vg.setLeafSize(0.01f, 0.01f, 0.01f);
    vg.filter(*cloud_filtered);
    std::cout << "PointCloud after filtering has: " << cloud_filtered->points.size() << " data points." << std::endl;
}

void transformCloud(pcl::PointCloud<pcl::PointXYZRGB>::Ptr &cloud, pcl::PointCloud<pcl::PointXYZRGB>::Ptr &final) {
    //pcl::PointCloud<pcl::PointXYZRGB>::Ptr  cloud (&output2_pcl);
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr  ground (new pcl::PointCloud<pcl::PointXYZRGB>);
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr  centeredcloud (new pcl::PointCloud<pcl::PointXYZRGB>);
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr  groundTransformed (new pcl::PointCloud<pcl::PointXYZRGB>);
    //pcl::PointCloud<pcl::PointXYZRGB>::Ptr  final (new pcl::PointCloud<pcl::PointXYZRGB>);

    pcl::ModelCoefficients::Ptr coefficients (new pcl::ModelCoefficients);
    pcl::PointIndices::Ptr floor_inliers (new pcl::PointIndices);
    pcl::SACSegmentation<pcl::PointXYZRGB>  seg;
    seg.setOptimizeCoefficients (true);
    seg.setModelType (pcl::SACMODEL_PLANE);
    seg.setMethodType (pcl::SAC_RANSAC);
    seg.setDistanceThreshold (0.01);
    seg.setInputCloud (cloud);
    seg.segment (*floor_inliers, *coefficients);

    pcl::ExtractIndices<pcl::PointXYZRGB> extract;
    extract.setInputCloud(cloud);
    extract.setIndices(floor_inliers);
    extract.setNegative(false);
    extract.filter(*ground);

    double normalLength = sqrt((coefficients->values[0] * coefficients->values[0]) + (coefficients->values[1] * coefficients->values[1]) + (coefficients->values[2] * coefficients->values[2]));

    pcl::PointXYZ normalXY;
    pcl::PointXYZ normalGround;
    pcl::PointXYZ result;
    normalXY.x = 0.0;
    normalXY.y = 0.0;
    normalXY.z = 1.0;

    normalGround.x = coefficients->values[0]/normalLength;
    normalGround.y = coefficients->values[1]/normalLength;
    normalGround.z = coefficients->values[2]/normalLength;

    result.x = normalGround.y * normalXY.z - normalXY.y*normalGround.z;
    result.y = normalXY.x*normalGround.z - normalGround.x*normalXY.z;
    result.z = normalGround.x*normalXY.y - normalGround.y*normalXY.x;
    double resultLenght = sqrt((result.x * result.x) + (result.y * result.y) +(result.z * result.z));
    result.x /=resultLenght;
    result.y /=resultLenght;
    result.z /=resultLenght;
    double theta = std::acos(normalGround.z/sqrt((normalGround.x* normalGround.x)+(normalGround.y*normalGround.y)+(normalGround.z*normalGround.z)));
    //std::cout << "The crossproduct is " << result.x << " "<< result.y<< " "<< result.z <<std::endl; 
    //std::cout << "norm " << std::sqrt((result.x * result.x  )+(result.y * result.y)+ (result.z * result.z))<< std::endl; 

    double xx = (result.x * result.x) + ((1 - (result.x * result.x)) * std::cos(theta));
    double xy = (result.x * result.y)* (1 - std::cos(theta)) - (result.z * std::sin(theta));
    double xz = (result.x * result.z)* (1 - std::cos(theta)) + (result.y * std::sin(theta));
    double yx = (result.x * result.y)* (1 - std::cos(theta)) + (result.z * std::sin(theta));
    double yy = (result.y * result.y) + ((1 - (result.y * result.y)) * std::cos(theta));
    double yz = (result.y * result.z)* (1 - std::cos(theta)) - (result.x * std::sin(theta));
    double zx = (result.x * result.z)* (1 - std::cos(theta)) - (result.y * std::sin(theta));
    double zy = (result.y * result.z)* (1 - std::cos(theta)) + (result.x * std::sin(theta));
    double zz = (result.z * result.z) + ((1 - (result.z * result.z)) * std::cos(theta));
    Eigen::Matrix4f transformXY;
    transformXY = Eigen::Matrix4f::Identity();

    transformXY (0,0) = xx; //cos (theta);
    transformXY (0,1) = xy;// -sin(theta)
    transformXY (0,2) = xz;
    transformXY (1,0) = yx; // sin (theta)
    transformXY (1,1) = yy;
    transformXY (1,2) = yz;
    transformXY (2,0) = zx ;
    transformXY (2,1) = zy;
    transformXY (2,2) = zz;
    pcl::transformPointCloud (*cloud, *final, transformXY);
//    pcl::transformPointCloud (*cloud, *centeredcloud, transformXY);
//    pcl::transformPointCloud (*ground, *groundTransformed, transformXY);
//
//    Eigen::Vector4f xyz_centroid;
//    // Estimate the XYZ centroid
//    pcl::compute3DCentroid (*groundTransformed, xyz_centroid);
//    std::cout << "The centroid is x " << xyz_centroid[0] << " y : "<< xyz_centroid[1] << " z " << xyz_centroid[2] << std::endl;
//    Eigen::Matrix4f translationTransform = Eigen::Matrix4f::Identity();
//    translationTransform = Eigen::Matrix4f::Identity();
////    translationTransform (0,3) = - xyz_centroid[0];
////    translationTransform (1,3) = - xyz_centroid[1];
////    translationTransform (2,3) = - xyz_centroid[2];
//    pcl::transformPointCloud (*centeredcloud, *final, translationTransform);
}


void cloud_cb1(const sensor_msgs::PointCloud2ConstPtr& input1) {
    sensor_msgs::PointCloud2 transformed_input;
    ros::NodeHandle n;
    listener->waitForTransform("/marker", (*input1).header.frame_id, (*input1).header.stamp, ros::Duration(1.0));
    //sensor_msgs::PointCloud &pcout;
    pcl_ros::transformPointCloud("/marker", *input1, transformed_input, *listener);
    pcl::fromROSMsg(transformed_input, output1_pcl);
    //*output_pcl = *output1_pcl;
    //pcl::copyPointCloud(*output1_pcl, *output_pcl);

    sub1.shutdown();
    sub2 = n.subscribe<sensor_msgs::PointCloud2>("art/n2/depth_registered/points", 1, cloud_cb2);
}

void cloud_cb2(const sensor_msgs::PointCloud2ConstPtr& input2) {
    sensor_msgs::PointCloud2 transformed_input;
    sensor_msgs::PointCloud2 output;
    ros::NodeHandle n;
    listener->waitForTransform("/marker", (*input2).header.frame_id, (*input2).header.stamp, ros::Duration(1.0));
    //sensor_msgs::PointCloud &pcout;
    pcl_ros::transformPointCloud("/marker", *input2, transformed_input, *listener);
    pcl::fromROSMsg(transformed_input, output2_pcl);


    //TODO kdyby to zarovnavani cloudu delalo bordel, tak tento blok zakomentuj
    if(!output1_pcl.empty() && !output2_pcl.empty()) {
        pcl::PointCloud<pcl::PointXYZRGB>::Ptr output1_pcl_transformed(new pcl::PointCloud<pcl::PointXYZRGB>);
        pcl::PointCloud<pcl::PointXYZRGB>::Ptr output2_pcl_transformed(new pcl::PointCloud<pcl::PointXYZRGB>);
        pcl::PointCloud<pcl::PointXYZRGB>::Ptr output1_pcl_ptr;
        output1_pcl_ptr = boost::shared_ptr<pcl::PointCloud<pcl::PointXYZRGB> >(
                new pcl::PointCloud<pcl::PointXYZRGB>(output1_pcl));
        pcl::PointCloud<pcl::PointXYZRGB>::Ptr output2_pcl_ptr;
        output2_pcl_ptr = boost::shared_ptr<pcl::PointCloud<pcl::PointXYZRGB> >(
                new pcl::PointCloud<pcl::PointXYZRGB>(output2_pcl));
        transformCloud(output1_pcl_ptr, output1_pcl_transformed);
        transformCloud(output2_pcl_ptr, output2_pcl_transformed);

        *output2_pcl_transformed += *output1_pcl_transformed;
        pcl::toROSMsg(*output2_pcl_transformed, output);
        pub.publish(output);

        sub2.shutdown();
        sub1 = n.subscribe<sensor_msgs::PointCloud2>("art/n1/depth_registered/points", 1, cloud_cb1);
    }

    
// ICP shit
//    if(!output1_pcl.empty() && !output2_pcl.empty()) {
//        pcl::PointCloud<pcl::PointXYZRGB> output1_pcl_local, output2_pcl_local;
//        pcl::copyPointCloud(output1_pcl, output1_pcl_local);
//        pcl::copyPointCloud(output2_pcl, output2_pcl_local);
//        pcl::PointCloud<pcl::PointXYZRGB>::Ptr ptrCloud1(&output1_pcl_local);
//        pcl::PointCloud<pcl::PointXYZRGB>::Ptr ptrCloud2(&output2_pcl_local);
//
//        std::vector<int> indices;
//        pcl::removeNaNFromPointCloud(*ptrCloud1, *ptrCloud1, indices);
//        pcl::removeNaNFromPointCloud(*ptrCloud2, *ptrCloud2, indices);
//
//        filterCloud(ptrCloud1, ptrCloud1);
//        filterCloud(ptrCloud2, ptrCloud2);
//
//        pcl::PCDWriter writer;
//        writer.write<pcl::PointXYZRGB>("cloud1.pcd", *ptrCloud1, false);
//        writer.write<pcl::PointXYZRGB>("cloud2.pcd", *ptrCloud2, false);
//
//        std::cout << "ICP START" << std::endl;
//
//        // The Iterative Closest Point algorithm
//        pcl::IterativeClosestPoint<pcl::PointXYZRGB, pcl::PointXYZRGB> icp;
//        icp.setMaximumIterations(1);
//        icp.setInputSource(ptrCloud1);
//        icp.setInputTarget(ptrCloud2);
//        icp.align(*ptrCloud1);
//
//        std::cout << "ICP DONE" << std::endl;
//
//        *ptrCloud2 += *ptrCloud1;
//        pcl::toROSMsg(*ptrCloud2, output);
//        pub.publish(output);
//
//        writer.write<pcl::PointXYZRGB>("cloud_merged.pcd", *ptrCloud2, false);
//
//
//
//    }
//    pcl::PointCloud<pcl::PointXYZRGB>::Ptr ptrCloud1(&output1_pcl);
//    pcl::PointCloud<pcl::PointXYZRGB>::Ptr ptrCloud2(&output2_pcl);
//    filterCloud(ptrCloud1, ptrCloud1);
//    filterCloud(ptrCloud2, ptrCloud2);


//TODO a toto odkomentuj
//    output2_pcl += output1_pcl;
//    pcl::toROSMsg(output2_pcl, output);
//    pub.publish(output);
//
//    sub2.shutdown();
//    sub1 = n.subscribe<sensor_msgs::PointCloud2>("art/n1/depth_registered/points", 1, cloud_cb1);
}

int main (int argc, char **argv)
{
    ros::init (argc, argv, "get_pointclouds_delay");
    ros::NodeHandle n;
  //  ARPublisher ar_kinect (n);

    listener = new tf::TransformListener();
    sub1 = n.subscribe("art/n1/depth_registered/points", 1, cloud_cb1);

    pub = n.advertise<sensor_msgs::PointCloud2>("/kinect_merged/depth_registered/points", 1);
    ros::spin ();

    return 0;
}
